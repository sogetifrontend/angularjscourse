'use strict';

var gulp = require('gulp'),
    karma = require('karma').server,
    $ = require('gulp-load-plugins')({ pattern: ['gulp-*', 'del']});

gulp.task('default', ['connect', 'watch'], function () {
    gulp.src(['src/**/*.js', '!src/**/*.spec.js'])

});

gulp.task('html', function () {
    gulp.src('src/*.html')
        .pipe($.connect.reload());
});

gulp.task('js', function () {
    gulp.src('src/app/**/*.js')
        .pipe($.connect.reload());
});

gulp.task('connect', function () {
    $.connect.server({
        root: ['./src'],
        livereload: true
    });
});

gulp.task('watch', function () {
    gulp.watch(['src/*.html'], ['html']);
    gulp.watch(['src/app/**/*.js'], ['js']);
});

gulp.task('lint', function () {
    gulp.src(['src/**/*.js'])
        .pipe($.jshint({"browser": true, "devel": true, "globalstrict": true, "globals": {"_": false, "$": false, "jasmine": false, "describe": false, "it": false, "expect": false, "beforeEach": false, "afterEach": false, "spyOn": false, "inject": false, "module": false, "angular": false}}))
        .pipe($.jshint.reporter('jshint-stylish'));
});