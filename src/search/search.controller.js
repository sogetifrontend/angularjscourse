'use strict';

angular.module('JukeTubeApp.search')
    .controller('SearchController', [
    	'$http',
    	'Results',
    	'$log',
        function search($http, Results, $log) {
            var vm = this;
            vm.query = null;

            vm.search = function () {
                $http.get('https://www.googleapis.com/youtube/v3/search', {
                    params: {
                        key: 'AIzaSyCViijCd2flYo2jIX0NMe3XdLc2ila97uA',
                        type: 'video',
                        maxResults: '8',
                        part: 'id,snippet',
                        fields: 'items/id,items/snippet/title,items/snippet/description,items/snippet/thumbnails/default,items/snippet/channelTitle',
                        q: vm.query
                    }
                })
                    .success(function (data) {
                        Results.listResults(data);
                        $log.info(data);
                    })
                    .error(function () {
                        $log.info('Search error');
                    });
            };
        }
    ]);