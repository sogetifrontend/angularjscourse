'use strict';

angular.module('JukeTubeApp', [
    'ui.router',
    'JukeTubeApp.search',
    'JukeTubeApp.results'
])
    .config([
        '$httpProvider',
        '$stateProvider',
        '$urlRouterProvider',
        function ($httpProvider, $stateProvider, $urlRouterProvider) {
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            $urlRouterProvider.otherwise('/upcoming');

            $stateProvider
                .state('root', {
                    abstract: true,
                    views: {
                        'search@': {
                            templateUrl: 'search/search.view.html',
                            controller: 'SearchController as searchCtrl'
                        },
                        'results@': {
                            templateUrl: 'results/results.view.html',
                            controller: 'ResultsController as resultsCtrl'
                        }
                    }
                })
                .state('root.upcoming', {
                    url: '^/upcoming'
                });
        }
    ]);

