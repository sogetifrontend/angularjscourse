'use strict';

angular.module('JukeTubeApp.results')
    .controller('ResultsController', [
        'Results',
        function results(Results) {
            var vm = this;
            vm.service = Results;
        }
    ]);