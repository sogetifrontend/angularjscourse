'use strict';

angular.module('JukeTubeApp.results')
    .service('Results', [function ResultsService() {
        var service = this;

        service.results = [];

		service.listResults = function (data) {
			var results = [];
			data.items.forEach (function (item) {
				results.push({
					id: item.id.videoId,
				    title: item.snippet.title,
				    description: item.snippet.description,
				    thumbnail: item.snippet.thumbnails.default.url,
				    author: item.snippet.channelTitle
			    });
			});
			service.results = results;
		}
    }]);